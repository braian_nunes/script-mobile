#!/bin/bash

export user_language=pt
export user_country=BR

template_start_method() {
  JAR=$1
  PROPERTIES_FILE=$2
  echo subindo serviço com o jar $JAR 
  java -jar $JAR -Duser.language=pt -Duser.country=BR $PROPERTIES_FILE &

  # java -jar sippe-app-processor-mob.jar --server.port=8088 --url.beneficio.base=http://sa-scs-hml-04:9390/api/prepaidtype --url.mob.base=http://localhost:8088/api

  # Criando diretório temporário
  # mkdir -p /tmp/microservice/pids
  # gerando pid do processo para o kill
  # ps aux | grep java | grep $JAR | grep -v grep | awk -F' ' '{print $2}' > /tmp/microservice/pids/$JAR.pid &
}

# Services
start_ms() {
  template_start_method sippe-app-processor-mob.jar --spring.config.location=file:/opt/aplic/config/sippe-processor-mob.properties
}

start_as() {
  template_start_method sippe-app-processor-mob-as.jar --spring.config.location=file:/opt/aplic/config/sippe-processor-mob-as.properties
}

kill_ms() {
	kill -9 $(ps aux | grep java | grep sippe-app-processor-mob.jar | grep -v grep | awk -F ' ' '{print $2}')
}

kill_as() {
	kill -9 $(ps aux | grep java | grep sippe-app-processor-mob-as.jar | grep -v grep | awk -F ' ' '{print $2}')
}

restart_ms() {
  kill_ms
  start_ms 
}

restart_as() {
  kill_as
  start_as 
}

# if condition
if [ "$1" = "start_ms" ] ; then
  echo start microservice_mob
  start_ms

elif [ "$1" = "start_as" ] ; then
  echo starting authorization_server
  start_as

elif [ "$1" = "restart_ms" ] ; then
  echo restarting microservice_mob
  restart_ms

elif [ "$1" = "restart_as" ] ; then
  echo restarting authorization_server
  restart_as

elif [ "$1" = "kill_ms" ] ; then
  echo Killing microservice_mob process
  kill_ms

elif [ 	"$1" = "kill_as" ] ; then
  echo Killing authorization_server process
  kill_as

else
  # filename="${1##*/}";filename="${filename%.*}"
  # echo $filename
  echo "Informe pelo menos um parâmetro: start_ms, start_as, restart_ms, restart_as, kill_ms ou kill_as"
fi
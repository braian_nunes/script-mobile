#!/bin/bash

# Host remoto
HOST='vm-uat-ac-as-01'

# Diretório do script no ambiente remoto
SRC_SCRIPT='cd /opt/aplic/; ./mobscript.sh'

# Obtém o usuário que está logado na máquina
USER_ACTIVE="$(id -un)"

# Diretório de deploy da aplicação
SRC_DEPLOY='/opt/aplic'

# Diretório temporário para armazenar os artefatos
TMP_DIR=/tmp/microservice/mobile/coopcerto

template_deploy_method() {
  URL_GIT=$1
  BRANCH=$2
  DIR=$3
  JAR=$4

  # Criando diretório temporário
  mkdir -p $TMP_DIR
  cd $TMP_DIR

  git clone $URL_GIT
  cd $DIR
  git checkout $BRANCH
  git pull
  # -Dmaven.test.skip=true -Dmaven.javadoc.skip=true
  mvn clean package -Dmaven.test.skip=true -Dmaven.javadoc.skip=true
  cd ..
  mv $DIR/target/$JAR .
  rm -rf $DIR
}

transfer_artifact() {
    echo "Efetuando transferência do artefato $1 para o servidor $HOST"
    scp $TMP_DIR/$1 $USER_ACTIVE@$HOST:$SRC_DEPLOY
}

# Services
deploy_ms() {
    JAR=sippe-app-processor-mob.jar
    template_deploy_method http://$USER_ACTIVE@ic.cabal.com.br/stash/scm/pm/sippe-processor-mob.git develop sippe-processor-mob $JAR
    transfer_artifact $JAR
    ssh $USER_ACTIVE@$HOST "$SRC_SCRIPT kill_ms; ($SRC_SCRIPT start_ms &) ; (exit)"
}

deploy_as() {
    JAR=sippe-app-processor-mob-as.jar
    template_deploy_method http://$USER_ACTIVE@ic.cabal.com.br/stash/scm/pmc/sippe-processor-mob-as.git develop sippe-processor-mob-as $JAR    
    transfer_artifact $JAR
    restart_as
}

restart_ms() {
    ssh $USER_ACTIVE@$HOST "$SRC_SCRIPT kill_ms; ($SRC_SCRIPT start_ms &) ; (exit)"
}

restart_as() {
    ssh $USER_ACTIVE@$HOST "$SRC_SCRIPT kill_as; ($SRC_SCRIPT start_as &) ; (exit)"
}

# Validação dos parâmetros de entrada
if [ "$1" = "deploy_ms" ] ; then
  echo deploying microservice_mob
  deploy_ms

elif [ "$1" = "deploy_as" ] ; then
  echo deploying authorization_server
  deploy_as

elif [ "$1" = "restart_ms" ] ; then
  echo restarting microservice_mob
  restart_ms

elif [ "$1" = "restart_as" ] ; then
  echo restarting authorization_server
  restart_as

else
  echo "Informe pelo menos um parâmetro: start_ms, start_as, restart_ms, restart_as, kill_ms ou kill_as"
fi